﻿using PinPaymentsSln.share.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace PinPaymentsSln.share.Services
{
    [Serializable]
    public class PinException : ApplicationException
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public PinError PinError { get; set; }

        public PinException()
        {
        }

        public PinException(HttpStatusCode httpStatusCode, PinError pinError, string message)
            : base(message)
        {
            HttpStatusCode = httpStatusCode;
            PinError = pinError;
        }
    }
}
