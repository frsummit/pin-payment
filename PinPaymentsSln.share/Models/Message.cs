﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class Message
    {
        public string code { get; set; }
        public string message { get; set; }
        public string param { get; set; }
    }
}
