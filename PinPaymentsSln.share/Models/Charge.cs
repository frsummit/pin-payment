﻿using Newtonsoft.Json;
using PinPaymentsSln.share.Models.Actions;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class Charges : PinError
    {
        public Charge[] Response { get; set; }
        public int Count { get; set; }
        public Pagination Pagination { get; set; }
    }

    public class ChargeResponse : PinError
    {
        [JsonProperty("response")]
        public Charge Charge { get; set; }
    }

    public class PostCharge
    {

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        private string _ip_address { get; set; }

        [JsonProperty("ip_address")]
        public string IPAddress 
        {
            get
            {
                if (_ip_address == null)
                {
                    return "127.0.0.1";
                }
                return _ip_address;
            }
            set { _ip_address = value; }
        }

        private string _currency { get; set; }

        [JsonProperty("currency")]
        public string Currency
        {
            get
            {
                if (_currency == null)
                {
                    return "AUD";
                }
                return _currency;
            }
            set { _currency = value; }
        }

        private Card _card;

        public Card Card
        {
            get { return _card; }
            set
            {
                if (value == null)
                {
                    _card = new Card();
                }
                else
                {
                    _card = value;
                }

            }
        }
        public PostCharge()
        {
            _card = new Card();
        }

        [JsonProperty("card[number]")]
        public string CardNumber { get { return Card.CardNumber; } set { Card.CardNumber = value; } }

        [JsonIgnore]
        public string DisplayNumber { get { return Card.DisplayNumber; } }

        [JsonProperty("card[expiry_month]")]
        public string ExpiryMonth { get { return Card.ExpiryMonth; } set { Card.ExpiryMonth = value; } }

        [JsonProperty("card[expiry_year]")]
        public string ExpiryYear { get { return Card.ExpiryYear; } set { Card.ExpiryYear = value; } }

        [JsonProperty("card[cvc]")]
        public string CVC { get { return Card.CVC; } set { Card.CVC = value; } }

        [JsonProperty("card[name]")]
        public string Name { get { return Card.Name; } set { Card.Name = value; } }

        [JsonProperty("card[address_line1]")]
        public string Address1 { get { return Card.Address1; } set { Card.Address1 = value; } }

        [JsonProperty("card[address_line2]")]
        public string Address2 { get { return Card.Address2; } set { Card.Address2 = value; } }

        [JsonProperty("card[address_city]")]
        public string City { get { return Card.City; } set { Card.City = value; } }

        [JsonProperty("card[address_postcode]")]
        public string Postcode { get { return Card.Postcode; } set { Card.Postcode = value; } }

        [JsonProperty("card[address_state]")]
        public string State { get { return Card.State; } set { Card.State = value; } }

        [JsonProperty("card[address_country]")]
        public string Country { get { return Card.Country; } set { Card.Country = value; } }

        //[JsonIgnore]
        //public Card Card { get; set; }

        [JsonIgnore]
        public string CardToken { get; set; }

        [JsonIgnore]
        public string CustomerToken { get; set; }

    }

    public class ChargeDetail
    {
        public Charge Response { get; set; }
    }
    public class Charge
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("amount")]
        public decimal amount;
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("ip_address")]
        public string IP_address { get; set; }

        [JsonProperty("created_at")]
        public DateTime Created { get; set; }

        [JsonProperty("error_message")]
        public string ErrorMessage { get; set; }

        [JsonProperty("status_message")]
        public string Status { get; set; }

        [JsonProperty("card_token")]
        public string Card_Token { get; set; }

        [JsonProperty("customer_token")]
        public string Customer_Token { get; set; }
        public Card Card { get; set; }

        [JsonProperty("amount_refunded")]
        public decimal amount_refunded;
        public decimal Amount_refunded
        {
            get { return amount_refunded; }
            set { amount_refunded = value; }
        }

        //[JsonProperty("total_fees")]
        //public decimal total_fees;
        //public decimal Total_fees
        //{
        //    get { return total_fees; }
        //    set { total_fees = value; }
        //}

        //[JsonProperty("merchant_entitlement")]
        //public decimal merchant_entitlement;
        //public decimal Merchant_entitlement
        //{
        //    get { return merchant_entitlement; }
        //    set { merchant_entitlement = value; }
        //}

        [JsonProperty("refund_pending")]
        public bool Refund_pending { get; set; }

        [JsonProperty("authorisation_expired")]
        public bool Authorisation_expired { get; set; }

        [JsonProperty("captured")]
        public bool Captured { get; set; }

        [JsonProperty("captured_at")]
        public DateTime Captured_at { get; set; }

        [JsonProperty("settlement_currency")]
        public string Settlement_currency { get; set; }

        [JsonProperty("active_chargebacks")]
        public bool Active_chargebacks { get; set; }

    }
}
