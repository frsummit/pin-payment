﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class Urls
    {
        public static string Card
        {
            get { return BaseUrl + "/1/cards"; }
        }

        public static string ChargesSearch
        {
            get { return BaseUrl + "/1/charges/search"; }
        }

        public static string Charge
        {
            get { return BaseUrl + "/1/charges"; }
        }

        public static string Charges
        {
            get { return BaseUrl + "/1/charges/"; }
        }

        public static string CustomerAdd
        {
            get { return BaseUrl + "/1/customers"; }
        }

        public static string Customers
        {
            get { return BaseUrl + "/1/customers"; }
        }

        public static string GetCustomer
        {
            get { return BaseUrl + "/1/customers/{token}"; }
        }
        public static string CustomerCharges
        {
            get { return BaseUrl + "/1/customers/{token}/charges"; }
        }

        public static string Refund
        {
            get { return BaseUrl + "/1/charges/{token}/refunds"; }
        }

        public static string Plan
        {
            get { return BaseUrl + "/1/plans"; }
        }

        public static string Subscription
        {
            get { return BaseUrl + "/1/subscriptions"; }
        }
        private static string BaseUrl
        {
            get { return "https://test-api.pin.net.au"; }
        }
        public static string CustomerSubscription
        {
            get { return BaseUrl + "/1/customers/{token}/subscriptions"; }
        }
        public static string PlanSubscriptions
        {
            get { return BaseUrl + "/1/plans/{token}/subscriptions"; }
        }
        public static string SubscriptionDeatils
        {
            get { return BaseUrl + "/1/subscriptions/{token}"; }
        }
        public static string SubscriptionLedgers
        {
            get { return BaseUrl + "/1/subscriptions/{token}/ledger"; }
        }
    }
}
