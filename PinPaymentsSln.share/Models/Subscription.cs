﻿using Newtonsoft.Json;
using PinPaymentsSln.share.Models.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class Subscriptions : PinError
    {
        public Subscription[] Response { get; set; }
        public int Count { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class SubscriptionResponse : PinError
    {
        public Subscription Response { get; set; }
    }

    [JsonObject("Subscription")]
    public class Subscription
    {
        [Key]
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("next_billing_date")]
        public string Next_billing_date { get; set; }

        [Display(Name = "Subscribed")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [JsonProperty("active_interval_started_at")]
        public DateTime Active_interval_started_at { get; set; }

        [Display(Name = "Next Charge")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [JsonProperty("active_interval_finishes_at")]
        public DateTime Active_interval_finishes_at { get; set; }

        [JsonProperty("cancelled_at")]
        public string Cancelled_at { get; set; }

        [JsonProperty("created_at")]
        public string Created_at { get; set; }

        [JsonProperty("plan_token")]
        public string Plan_token { get; set; }

        [JsonProperty("customer_token")]
        public string Customer_token { get; set; }

        [JsonProperty("card_token")]
        public string Card_token { get; set; }
        public PlanResponse Plan { get; set; }
        public Customer Customer { get; set; }
        public SubscriptionLedgerResponse SubscriptionLedger { get; set; }
    }

    public class PostSubscription
    {
        [JsonProperty("plan_token")]
        public string plan_token { get; set; }

        [JsonProperty("customer_token")]
        public string customer_token { get; set; }

        [JsonProperty("card_token")]
        public string card_token { get; set; }

        [JsonProperty("include_setup_fee")]
        public bool include_setup_fee { get; set; }
    }

    public class SubscriptionLedgerResponse : PinError
    {
        public SubscriptionLedger[] Response { get; set; }
        public int Count { get; set; }
        public Pagination Pagination { get; set; }
    }

    public class SubscriptionLedger
    {
        [JsonProperty("created_at")]
        public DateTime Created_at { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("annotation")]
        public string Annotation { get; set; }
    }
}
