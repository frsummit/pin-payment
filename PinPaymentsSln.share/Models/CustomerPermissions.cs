﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class CustomerPermissions
    {
        [JsonProperty("")]
        public string Status { get; set; }
    }
}
