﻿using Newtonsoft.Json;
using PinPaymentsSln.share.Models.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class Plans : PinError
    {
        public Plan[] Response { get; set; }
        public int Count { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class PlanResponse : PinError
    {
        public Plan Response { get; set; }
    }

    [JsonObject("plan")]
    public class Plan
    {
        [Key]
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("created_at")]
        public DateTime Created { get; set; }

        [JsonProperty("trial_interval_unit")]
        public string Trial_interval_unit { get; set; }

        [JsonProperty("trial_interval")]
        public int Trial_interval { get; set; }

        [JsonProperty("intervals")]
        public int Intervals { get; set; }

        [JsonProperty("interval_unit")]
        public string Interval_unit { get; set; }

        [JsonProperty("interval")]
        public int Interval { get; set; }

        [JsonProperty("trial_amount")]
        public int Trial_amount { get; set; }

        [JsonProperty("setup_amount")]
        public int Setup_amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        //[JsonProperty("customer_permissions")]
        //public string Customer_permissions { get; set; }

        [JsonProperty("subscription_counts")]
        public SubscriptionCount Cubscription_counts { get; set; }
        public Subscriptions Subscriptions { get; set; }
        public Customers Customers { get; set; }
    }

    public class PostPlan
    {
        [JsonProperty("trial_interval_unit")]
        public string Trial_interval_unit { get; set; }

        [JsonProperty("trial_interval")]
        public int Trial_interval { get; set; }

        [JsonProperty("intervals")]
        public int Intervals { get; set; }

        [JsonProperty("interval_unit")]
        public string Interval_unit { get; set; }

        [JsonProperty("interval")]
        public int Interval { get; set; }

        [JsonProperty("trial_amount")]
        public int Trial_amount { get; set; }

        [JsonProperty("setup_amount")]
        public int Setup_amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("customer_permissions")]
        public CustomerPermissions Customer_permissions { get; set; }

    }
}
