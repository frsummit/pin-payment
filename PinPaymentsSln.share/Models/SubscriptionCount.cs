﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PinPaymentsSln.share.Models
{
    public class SubscriptionCount
    {
        [JsonProperty("trial")]
        public int Trial { get; set; }

        [JsonProperty("active")]
        public int Active { get; set; }

        [JsonProperty("cancelling")]
        public int Cancelling { get; set; }

        [JsonProperty("cancelled")]
        public int Cancelled { get; set; }
    }
}
