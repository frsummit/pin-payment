﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PinPaymentsSln.dev.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Card",
                columns: table => new
                {
                    Token = table.Column<string>(nullable: false),
                    CardNumber = table.Column<string>(nullable: true),
                    CVC = table.Column<string>(nullable: true),
                    DisplayNumber = table.Column<string>(nullable: true),
                    ExpiryMonth = table.Column<string>(nullable: true),
                    ExpiryYear = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Postcode = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Scheme = table.Column<string>(nullable: true),
                    APIKey = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Card", x => x.Token);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Token = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    CardToken = table.Column<string>(nullable: true),
                    CardNumber = table.Column<string>(nullable: true),
                    ExpiryMonth = table.Column<string>(nullable: true),
                    ExpiryYear = table.Column<string>(nullable: true),
                    CVC = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Postcode = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Token);
                    table.ForeignKey(
                        name: "FK_Customer_Card_CardToken",
                        column: x => x.CardToken,
                        principalTable: "Card",
                        principalColumn: "Token",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customer_CardToken",
                table: "Customer",
                column: "CardToken");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Card");
        }
    }
}
