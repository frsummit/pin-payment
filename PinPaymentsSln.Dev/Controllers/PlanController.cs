﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PinPaymentsSln.share.Models;
using PinPaymentsSln.share.Services;

namespace PinPaymentsSln.dev.Controllers
{
    public class PlanController : Controller
    {
        private readonly PinPaymentDbContext _context;
        PinService service = new PinService("KZ7j0P1wso3rZTHvspZ-Rg");
        //PinService service = new PinService("U3P9_fpJdgfPI8F9vhagSA");

        public PlanController(PinPaymentDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var plan = service.Plans();
            return View(plan.Response.ToList());
        }

        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            PlanResponse plan = service.Plan(id);
            var subscriberResponse = service.PlanSubscriptions(id);
            foreach (var subscription in subscriberResponse.Response)
            {
                subscription.Customer = service.Customer(subscription.Customer_token);
            }
            plan.Response.Subscriptions = subscriberResponse;

            //var customer = service.Customer(id);
            //Customer customers = customer;

            //Customer customer = service.Customer(id);
            //var subscriberResponses = service.CustomerSubscriptions(id);
            //customer.Subscriptions = subscriberResponses;
            //var customerResponse = service.Customer(subscriberResponse.Response);
            if (plan == null)
            {
                return NotFound();
            }

            return View(plan.Response);
            //return View(customers);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Name, Amount, Currency, Interval, Interval_unit, Intervals, Setup_amount, Trial_amount, Trial_interval, Trial_interval_unit")] PostPlan plans)
        {
            if (ModelState.IsValid)
            {
                service.CreatePlan(plans);
                return RedirectToAction(nameof(Index));
            }
            return View(plans);
        }

        private bool CustomerExists(string id)
        {
            return _context.Customer.Any(e => e.Token == id);
        }
    }
}