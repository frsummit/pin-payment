﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PinPaymentsSln.share.Services;
using PinPaymentsSln.share.Models;

namespace PinPaymentsSln.dev.Controllers
{
    public class SubscriptionController : Controller
    {
        private readonly PinPaymentDbContext _context;
        PinService service = new PinService("KZ7j0P1wso3rZTHvspZ-Rg");
        //PinService service = new PinService("U3P9_fpJdgfPI8F9vhagSA");

        public SubscriptionController(PinPaymentDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        // GET: Subscription/Details/5
        public IActionResult Details(string id)
        {
            //Subscription subscrip = new Subscription();
            //subscrip.Token = id;
            //var respSubscription = service.SubscriptionDetails(id);

            if (id == null)
            {
                return NotFound();
            }
            var subscription = service.SubscriptionDetails(id);
            var customer = service.Customer(subscription.Response.Customer_token);
            var plan = service.Plan(subscription.Response.Plan_token);
            var ledgers = service.SubscriptionLedgers(id);
            subscription.Response.Customer = customer;
            subscription.Response.Plan = plan;
            subscription.Response.SubscriptionLedger = ledgers;
            Subscription subscriptions = subscription.Response;

            if (customer == null)
            {
                return NotFound();
            }

            //return View(customer);


            return View(subscriptions);
        }
    }
}