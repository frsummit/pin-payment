﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PinPaymentsSln.share.Models;
using PinPaymentsSln.share.Services;

namespace PinPaymentsSln.dev.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class CustomerController : Controller
    {
        private readonly PinPaymentDbContext _context;
        PinService service = new PinService("KZ7j0P1wso3rZTHvspZ-Rg");
        //PinService service = new PinService("U3P9_fpJdgfPI8F9vhagSA");

        public CustomerController(PinPaymentDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var customer = service.Customers();
            return View(customer.Customer.ToList());
        }

        public IActionResult CustomerSubscription(string id)
        {
            var plan = service.Plans();
            ViewBag.CustomerToken = id;
            return View(plan.Response.ToList().Take(4));
        }

        public IActionResult Subscription(string plan, string id)
        {
            PostSubscription subscription = new PostSubscription();
            Customer customer = service.Customer(id);
            subscription.plan_token = plan;
            subscription.customer_token = id;
            subscription.card_token = customer.Card.Token;
            var subscriptions = service.CreateSubscription(subscription);
            var customers = service.Customer(subscriptions.Response.Customer_token);
            var plans = service.Plan(subscriptions.Response.Plan_token);
            var ledgers = service.SubscriptionLedgers(id);
            subscriptions.Response.Customer = customers;
            subscriptions.Response.Plan = plans;
            subscriptions.Response.SubscriptionLedger = ledgers;
            Subscription subscriptionsb = subscriptions.Response;
            return View(subscriptionsb);
        }


        public IActionResult Charges(string id)
        {
            PostCharge charge = new PostCharge();
            Customer customer = service.Customer(id);
            charge.CustomerToken = id;
            charge.Email = customer.Email;
            charge.Card = customer.Card;
            return View(charge);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CustomerCharge([Bind("Amount,Description,Email,CustomerToken,DisplayNumber")] PostCharge charge)
        {
            var response = service.Charge(charge);
            return RedirectToAction(nameof(Index));
            //return View(charge);
        }



        // GET: Customer/Details/5
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var customer = await _context.Customer
            //    .FirstOrDefaultAsync(m => m.Token == id);
            Customer customer = service.Customer(id);
            var responsess = service.CustomerCharges(id);
            var customerSubscription = service.CustomerSubscriptions(id);
            foreach (var subscription in customerSubscription.Response)
            {
                subscription.Plan = service.Plan(subscription.Plan_token);
            }
            customer.Charges = responsess;
            customer.Subscriptions = customerSubscription;
            //foreach(var subscription in customer.Subscriptions.Response)
            //{
            //    DateTime startDate = Convert.ToDateTime(subscription.Active_interval_started_at);
            //    DateTime endDate = Convert.ToDateTime(subscription.Active_interval_finishes_at);
            //}

            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // GET: Customer/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Email,Token,DateCreated,CardNumber,ExpiryMonth,ExpiryYear,CVC,Name,Address1,Address2,City,Postcode,State,Country")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                service.CustomerAdd(customer);
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
            //else
            //{
            //    return View(customer);
            //}
        }

        private bool CustomerExists(string id)
        {
            return _context.Customer.Any(e => e.Token == id);
        }
    }
}
