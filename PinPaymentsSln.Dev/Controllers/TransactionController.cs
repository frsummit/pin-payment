﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PinPaymentsSln.share.Models;
using PinPaymentsSln.share.Services;

namespace PinPaymentsSln.dev.Controllers
{
    public class TransactionController : Controller
    {
        private readonly PinPaymentDbContext _context;
        PinService service = new PinService("KZ7j0P1wso3rZTHvspZ-Rg");
        //PinService service = new PinService("U3P9_fpJdgfPI8F9vhagSA");

        public TransactionController(PinPaymentDbContext context)
        {
            _context = context;
        }

        // GET: Transaction
        public IActionResult Index()
        {
            //return View(await _context.Charge.ToListAsync());

            var charges = service.Charges();
            return View(charges.Response.ToList());
        }

        // GET: Transaction/Details/5
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var charge = await _context.Charge
            //    .FirstOrDefaultAsync(m => m.Token == id);
            ChargeDetail charge = service.Charge(id);
            if (charge == null)
            {
                return NotFound();
            }

            return View(charge.Response);
        }

        // GET: Transaction/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Transaction/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Email,Description,Amount,Currency,IP_address,CardNumber,ExpiryMonth,ExpiryYear,CVC,Name,Address1,Address2,City,Postcode,State,Country")] PostCharge charge)
        {
          var response = service.Charge(charge);
          return RedirectToAction(nameof(Index));
            //return View(charge);
        }

        // GET: Transaction/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var charge = await _context.Charge.FindAsync(id);
            if (charge == null)
            {
                return NotFound();
            }
            return View(charge);
        }

        // POST: Transaction/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Token,Success,Amount,Currency,Description,Email,IP_address,Created,ErrorMessage,Status,Card_Token,Customer_Token,Amount_refunded,Total_fees,Merchant_entitlement,Refund_pending,Authorisation_expired,Captured,Captured_at,Settlement_currency,Active_chargebacks")] Charge charge)
        {
            if (id != charge.Token)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(charge);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChargeExists(charge.Token))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(charge);
        }

        // GET: Transaction/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var charge = await _context.Charge
                .FirstOrDefaultAsync(m => m.Token == id);
            if (charge == null)
            {
                return NotFound();
            }

            return View(charge);
        }

        // POST: Transaction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var charge = await _context.Charge.FindAsync(id);
            _context.Charge.Remove(charge);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ChargeExists(string id)
        {
            return _context.Charge.Any(e => e.Token == id);
        }
    }
}
