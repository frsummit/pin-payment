﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PinPaymentsSln.share.Models;

    public class PinPaymentDbContext : DbContext
    {
        public PinPaymentDbContext (DbContextOptions<PinPaymentDbContext> options)
            : base(options)
        {
        }

        public DbSet<PinPaymentsSln.share.Models.Customer> Customer { get; set; }
        public DbSet<PinPaymentsSln.share.Models.Charge> Charge { get; set; }
}
